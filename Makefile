#
# Install configuration files.
#
SHELL = /bin/bash
CFGDIR = $(HOME)/config
LOGDIR = $(HOME)/logs
CFGFILES = switches chaba-defaults sensors.yaml schedule.cron \
	sample_times.txt
.PHONY: install install-dirs

all: install install-dirs

install: $(CFGFILES)
	cp -v $^ $(CFGDIR)/

install-dirs:
	mkdir -p $(LOGDIR)/dataxfer
	mkdir -p $(LOGDIR)/sample